FROM node:8.16.0-alpine
MAINTAINER Mathys Laviéville (mathys.lavieville@epitech.eu)

RUN apk add --update \
    python \
    build-base \
    && rm -rf /var/cache/apk/*

WORKDIR /app

COPY package*.json ./

RUN npm i

COPY . .

EXPOSE 80 443 ${PORT}
CMD ["npm", "run", "start"]
