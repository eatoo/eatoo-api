# EATOO API

This API is based on [NestJS](https://nestjs.com/), a progressive Node.js framework built with Typescript for building efficient, reliable and scalable server-side applications.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development and watch mode
$ npm run start:dev

# debug and watch mode
$ npm run start:debug

# production mode with source files
$ npm run start:prod

# production mode with built files
$ npm run start
```

## Testing

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Building

```bash
$ npm run build
``` 

## Configuration

Many features can be configured thanks to the [config](https://www.npmjs.com/package/config) npm package.

All the defaults parameters are defined in `config/default.ts`.

These parameters are then merged depending on the `NODE_ENV` environment variable and the associated configuration file.

### Simple example with NODE_ENV=development

```js
// config/default.ts
module.exports = {
  helloWorld: "Hello World!",
  foo: "foo",
};
```

```js
// config/development.ts
module.exports = {
  helloWorld: "Hello Eatoo API!"
};
```

```js
// src/main.ts
import * as config from 'config';

console.log(config.get('helloWorld'));    // "Hello Eatoo API!"
console.log(config.get('foo'));           // "foo"
```   

### API Configuration in details

#### listenPort

Port the API will listen to. Note that the `PORT` environment variable will take precedence.

#### database.url

URL of the MongoDB database.

#### database.name

Name of the dabatase.

#### plugins.jwt.secret

Secret key used to sign JSON Web Token.

#### plugins.jwt.expiresIn

Expiration of the JSON Web Token.

#### plugins.swagger.title

Title displayed in Swagger Documentation.

#### plugins.swagger.version

Version of the documentation.

#### plugins.swagger.path

URL path to access Swagger Documentation.

#### plugins.rateLimit.windowMs

Interval of time, in ms.

#### plugins.rateLimit.max

Number of allowed request per IP per windowMs.

#### redis.host

Host of the Redis Microservice.

#### redis.port

Port of the Redis Microservice.

#### redis.debugFails

Enables logging when communication fails between API and the microservice.

#### redis.connectionRetries

- `false`: disables reconnection retries
- `true`: always retries to reconnect
- `<number>`: retries to reconnect at most n times

#### redis.enabled

Enables the communication with the Redis Microservice.

## Redis Microservice

When enabled, the API will emit events to the [Redis Microservice](https://gitlab.com/eatoo/eatoo-api-redis).

This microservice handles analytics and statistics.

## Continuous Integration / Continuous Delivery

#### Tests and Heroku
Any push on the `dev` branch triggers Units and End-To-End (E2E) tests.
If all tests were successful, the API is then deployed on Heroku (https://eip-eatoo.herokuapp.com).
Please note that API on that URL may not be stable and may contains some bugs.

#### Deployment on Azure

https://x2021eatoo636384406001.northeurope.cloudapp.azure.com

The API is deployed to Azure when a tag is pushed, with the `deploy.sh` script. The SSL certificate is auto-generated and renewed when needed. 

Example:
```bash
git tag 1.2.3
git push origin 1.2.3
```

This will trigger the build of the Docker Image in `registry.gitlab.com`.

Two tagged images will be created :
- `registry.gitlab.com/eatoo/eatoo-api:latest`
- `registry.gitlab.com/eatoo/eatoo-api:1.2.3`
