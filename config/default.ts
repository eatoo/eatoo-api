module.exports = {
  listenPort: 3000,
  database: {
    name: 'eatoo',
    url: 'mongodb+srv://eatoo:1hmR*q1x@eatoo-cluster-nndio.mongodb.net',
  },
  plugins: {
    jwt: {
      secret: 'hWCFaBxHGJ1y3eHZvaRAjOBPSOElWjvySWmIK8FPoHA=',
      expiresIn: 3600,
    },
    swagger: {
      title: 'Eatoo API Documentation',
      version: '1.7',
      path: 'doc',
    },
    rateLimit: {
      windowMs: 15 * 60 * 1000,
      max: 1000,
    },
  },
  tests: {
    newUserAdmin: false,
  },
  redis: {
    host: '127.0.0.1',
    port: 6373,
    debugFails: false,
    
    /**
     * Possible values:
     * - false: Never retry to reconnect
     * - true: Always retry to reconnect
     * - <number>: Retry n times to reconnect
     */
    connectionRetries: false,
  
    /**
     * Enable the communication within the Redis Microservice
     */
    enabled: true,
  },
};
