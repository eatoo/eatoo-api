module.exports = {
  database: {
    name: 'eatoo_dev',
  },
  tests: {
    newUserAdmin: false,
  },
  redis: {
    debugFails: true,
    connectionRetries: true,
  },
};
