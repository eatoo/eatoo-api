module.exports = {
  database: {
    name: 'eatoo',
  },
  redis: {
    host: 'eatoo-ms-redis',
    port: 6373,
    debugFails: false,
  },
};
