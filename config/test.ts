module.exports = {
  database: {
    name: 'eatoo_test',
  },
  redis: {
    enabled: false,
  }
};
