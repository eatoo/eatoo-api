#!/usr/bin/env bash

CONTAINER_NAME="eatoo-api"



docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
docker stop $CONTAINER_NAME && docker rm $CONTAINER_NAME || true
docker pull $CI_REGISTRY_IMAGE:latest
docker run \
       -d \
       --restart=on-failure \
       --env "VIRTUAL_HOST=x2021eatoo636384406001.northeurope.cloudapp.azure.com" \
       --env "LETSENCRYPT_HOST=x2021eatoo636384406001.northeurope.cloudapp.azure.com" \
       --env "LETSENCRYPT_EMAIL=mathys.lavieville@epitech.eu" \
       --env "API_VERSION=$CI_COMMIT_TAG" \
       --env "PORT=80" \
       --name $CONTAINER_NAME \
       $CI_REGISTRY_IMAGE:latest
