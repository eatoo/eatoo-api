import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthController } from './auth/auth.controller';
import { AuthService } from './auth/auth.service';
import { AuthModule } from './auth/auth.module';
import { conf } from './helper/helpers.methods';
import { HelperModule } from './helper/helper.module';
import { RedisModule } from './redis/redis.module';

const database = conf('database.name', 'eatoo');
const url = conf('database.url', '');
const path = `${url}/${database}?retryWrites=true`;

@Module({
  imports: [
    RedisModule,
    AuthModule,
    HelperModule,
    MongooseModule.forRoot(path, { useNewUrlParser: true, useFindAndModify: false }),
  ],
  controllers: [AppController, AuthController],
  providers: [
    AppService,
    AuthService,
  ],
})
export class AppModule {
}
