import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { MongooseModule, getModelToken } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { User } from './interfaces/user.interface';
import { conf } from '../helper/helpers.methods';
import UserSchema from './schemas/user.schema';

describe('Auth Controller', () => {
  let controller: AuthController;

  beforeEach(async () => {
    const userModel: User = {
      username: 'toto',
      password: 'toto',
      email: 'toto@eatoo.com',
      age: '21',
      roles: [],
    };
    const database = conf('database.name', 'eatoo');
    const url = conf('database.url', '');
    const path = `${url}/${database}?retryWrites=true`;
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        PassportModule,
        MongooseModule.forRoot(path, { useNewUrlParser: true }),
        MongooseModule.forFeature([
          {
            name: 'User',
            schema: UserSchema,
          },
        ]),
        JwtModule.register({
          secret: conf('plugins.jwt.secret', 'secret'),
          signOptions: {
            expiresIn: conf('plugins.jwt.expiresIn', 3600),
          },
        }),
      ],
      controllers: [AuthController],
      providers: [
        AuthService,
        {
          provide: getModelToken('User'),
          useValue: userModel,
        },
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
