import { Body, Controller, Get, HttpCode, HttpException, HttpStatus, Patch, Post, UnauthorizedException, UseGuards } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiExcludeEndpoint,
  ApiForbiddenResponse,
  ApiInternalServerErrorResponse,
  ApiResponse,
  ApiUnauthorizedResponse,
  ApiUseTags,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { Model } from 'mongoose';
import { AuthService } from './auth.service';
import { RegisterDto } from './dto/register.dto';
import { Role, User } from './interfaces/user.interface';
import { LoginDto } from './dto/login.dto';
import { AuthSuccess } from './interfaces/auth-success.interface';
import { RolesGuard } from './guards/roles.guard';
import { CurrentUser } from './decorators/current-user.decorator';
import { RedisService } from '../redis/redis.service';
import { UpdateDto } from './dto/update.dto';

@ApiUseTags('auth')
@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly redisService: RedisService,
    @InjectModel('User') private readonly userModel: Model<User>,
  ) {
  }
  
  /**
   * ! /AUTH/REGISTER
   * @param dto
   */
  @ApiResponse({ status: 201, description: 'Successful registration' })
  @ApiBadRequestResponse({ description: 'Bad request. Email or username has already been taken.' })
  @ApiInternalServerErrorResponse({ description: 'An error occurred on our side.' })
  @Post('register')
  async register(@Body() dto: RegisterDto): Promise<AuthSuccess> {
    const userModel = new this.userModel(dto);
    /* if (conf('tests.newUserAdmin', false)) {
      userModel.roles = [Role.ADMIN];
    } */
    const user = await userModel
      .save()
      .catch(error => {
        if (error.code === 11000) {
          if (error.errmsg.search('email_1 dup key') !== -1) {
            throw new HttpException('Email has already been taken.', HttpStatus.BAD_REQUEST);
          }
          if (error.errmsg.search('username_1 dup key') !== -1) {
            throw new HttpException('Login has already been taken.', HttpStatus.BAD_REQUEST);
          }
          throw new HttpException('Registration failed.', HttpStatus.BAD_REQUEST);
        }
        throw new HttpException('An error occurred on our side.', HttpStatus.INTERNAL_SERVER_ERROR);
      });
    this.redisService.emit<void>('ACCOUNT_CREATION');
    return {
      token: await this.authService.signJwt(user),
      user,
    };
  }
  
  /**
   * ! /AUTH/LOGIN
   * @param dto
   */
  @ApiResponse({ status: 200, description: 'Successful connection.' })
  @ApiBadRequestResponse({ description: 'Bad request.' })
  @ApiUnauthorizedResponse({ description: 'Authentication failed.' })
  @HttpCode(200)
  @Post('login')
  async login(@Body() dto: LoginDto): Promise<AuthSuccess> {
    this.redisService.emit<void>('ALL', {
      data: {
        toto: 'tata',
        tutu: 'titi',
      },
      dateIncrements: 'ymd',
    });
    const user = await this.authService.login(dto);
    if (!user) {
      throw new UnauthorizedException('Authentication failed.');
    }
    this.redisService.emit<void>('ACCOUNT_CONNECTION');
    return {
      token: await this.authService.signJwt(user),
      user,
    };
  }
  
  /**
   * ! /AUTH/ME
   * Route only available for authenticated user
   */
  @UseGuards(AuthGuard())
  @ApiBearerAuth()
  @ApiResponse({ status: 200, description: 'Returns the user corresponding to the provided JWT.' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized.' })
  @ApiForbiddenResponse({ description: 'Forbidden.' })
  @HttpCode(200)
  @Get('me')
  protected(@CurrentUser() user: User): User {
    this.redisService.emit<void>('ENDPOINT_ME');
    return user;
  }
  
  /**
   * ! /AUTH/ME
   * @todo Update : not in /auth
   */
  @UseGuards(AuthGuard())
  @ApiBearerAuth()
  @ApiResponse({ status: 200, description: 'Returns the updated user' })
  @Patch('me')
  @HttpCode(200)
  async updateUser(@Body() dto: UpdateDto, @CurrentUser() user: User): Promise<User> {
    this.redisService.emit<void>('ENDPOINT_PATCH_ME');
    const toto = await this.authService
      .update(dto, user)
      .catch(error => {
        if (error.code === 11000) {
          if (error.errmsg.search('email_1 dup key')) {
            throw new HttpException('email has already been taken.', HttpStatus.BAD_REQUEST);
          }
        }
        throw new HttpException('An error occurred on our side.', HttpStatus.INTERNAL_SERVER_ERROR);
      });
      toto.email = dto.email;
    return toto;
  }
  
  /**
   * Example only
   * Route only available for user with ADMIN role, and not documented in Swagger
   */
  @UseGuards(AuthGuard(), new RolesGuard([Role.ADMIN]))
  @ApiExcludeEndpoint()
  @Get('protectedadmin')
  protectedAdmin(): string {
    return 'OK !';
  }
}
