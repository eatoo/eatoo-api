import { Test, TestingModule } from '@nestjs/testing';
import { MongooseModule, getModelToken } from '@nestjs/mongoose';
import { JwtModule } from '@nestjs/jwt';
import { Model } from 'mongoose';
import { AuthService } from './auth.service';
import { User } from './interfaces/user.interface';
import UserSchema from './schemas/user.schema';
import { conf } from '../helper/helpers.methods';

describe('AuthService', () => {
  let service: AuthService;
  
  beforeEach(async () => {
    const userModel: User = {
      username: 'toto',
      password: 'toto',
      email: 'toto@eatoo.com',
      age: '21',
      roles: [],
    };
    const database = conf('database.name', 'eatoo');
    const url = conf('database.url', '');
    const path = `${url}/${database}?retryWrites=true`;
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(path, { useNewUrlParser: true }),
        MongooseModule.forFeature([
          {
            name: 'User',
            schema: UserSchema,
          },
        ]),
        JwtModule.register({
          secret: conf('plugins.jwt.secret', 'secret'),
          signOptions: {
            expiresIn: conf('plugins.jwt.expiresIn', 3600),
          },
        }),
      ],
      providers: [
        AuthService,
        {
          provide: getModelToken('User'),
          useValue: userModel,
        },
      ],
    }).compile();
    
    service = module.get<AuthService>(AuthService);
  });
  
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
