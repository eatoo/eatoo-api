import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { LoginDto } from './dto/login.dto';
import { User } from './interfaces/user.interface';
import * as bcrypt from 'bcrypt';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { UpdateDto } from './dto/update.dto';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel('User') private readonly userModel: Model<User>,
    private readonly jwtService: JwtService,
  ) {
  }
  
  login(data: LoginDto): Promise<User> {
    const fieldName = data.email ? 'email' : (data.username) ? 'username' : null;
    if (fieldName == null) {
      throw new HttpException('You must specify either an email or a username.', HttpStatus.BAD_REQUEST);
    }
    return this.userModel
      .findOne({ [fieldName]: data[fieldName] })
      .then((user: User) => {
        if (!user) {
          throw new HttpException(`Incorrect ${fieldName}.`, HttpStatus.UNAUTHORIZED);
        }
        if (!bcrypt.compareSync(data.password, user.password)) {
          throw new HttpException('Incorrect password.', HttpStatus.UNAUTHORIZED);
        }
        return user;
      });
  }
  
  async signJwt(user: User): Promise<string> {
    return this.jwtService.sign({
      _id: user._id,
      username: user.username,
    });
  }
  
  async update(data: UpdateDto, user: User): Promise<User> {
    return this.userModel.findOneAndUpdate({_id: user._id}, data, {returnNewDocument: false});
  }

  async validateJwt(payload: JwtPayload): Promise<User> {
    return await this.userModel.findOne({
      _id: payload._id,
      username: payload.username,
    });
  }
}
