import { SetMetadata } from '@nestjs/common';
import { Role } from '../interfaces/user.interface';

export const Roles = (...roles: Role[]) => SetMetadata('roles', roles);
