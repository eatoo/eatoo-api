import { IsEmail, IsNotEmpty, Length } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class RegisterDto {
  @ApiModelProperty({minimum: 3})
  @IsNotEmpty()
  @Length(3)
  readonly username: string;

  @ApiModelProperty()
  @IsEmail()
  @IsNotEmpty()
  readonly email: string;

  @ApiModelProperty({minimum: 6})
  @IsNotEmpty()
  @Length(6)
  readonly password: string;
}
