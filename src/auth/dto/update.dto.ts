import { IsEmail, IsNotEmpty, Length, IsOptional } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class UpdateDto {
  @ApiModelPropertyOptional()
  @IsEmail()
  @IsOptional()
  readonly email?: string;
}
