import { ExceptionFilter, Catch, HttpException, HttpStatus } from '@nestjs/common';
import { MongoError } from 'mongodb';

@Catch(MongoError)
export class MongoFilter implements ExceptionFilter {
  catch(exception: MongoError) {
    if (exception.code === 11000) {
      throw new HttpException('Incorrect password.', HttpStatus.UNAUTHORIZED);
    } else {
      throw new HttpException('An error occured', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
