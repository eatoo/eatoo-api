import { Logger, Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Role } from '../interfaces/user.interface';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly roles: Role[]) {
  }
  
  canActivate(context: ExecutionContext): boolean {
    /*const roles = this.reflector.get<Role[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }*/
    const request = context.switchToHttp().getRequest();
    const user = request.user;
    if (!user) {
      return true;
    }
    // const hasRole = () => user.roles.some((role) => roles.includes(role));
    const hasRole = () => this.roles.some(role => user.hasRole(role));
    return user && user.roles && hasRole();
  }
}
