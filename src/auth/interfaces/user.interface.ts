export enum Role {
  USER = 'USER',
  BUYER = 'BUYER',
  SELLER = 'SELLER',
  MODERATOR = 'MODERATOR',
  ADMIN = 'ADMIN',
  SUPERADMIN = 'SUPERADMIN',
}

export interface User {
  _id?: string;
  email: string;
  username: string;
  password: string;
  age?: string;
  roles: Role[];
}
