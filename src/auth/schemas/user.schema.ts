import * as mongoose from 'mongoose';
import * as bcrypt from 'bcrypt';
import { Role } from '../interfaces/user.interface';

const UserSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true,
    },
    username: {
      type: String,
      unique: true,
      required: true,
    },
    password: {
      type: String,
    },
    age: String,
    roles: Array,
  },
  {
    timestamps: true,
  },
);

UserSchema.pre('save', function(next) {
  if (this.isModified('password') || this.isNew) {
    this.password = bcrypt.hashSync(this.password, 10);
  }
  return next();
});

UserSchema.methods.toJSON = function() {
  const obj = this.toObject();
  delete obj.password;
  delete obj.roles;
  return obj;
};

UserSchema.methods.hasRole = function(role) {
  return this.roles.some(r => r === role);
};

export default UserSchema;
