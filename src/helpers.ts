import * as config from 'config';

export const conf = (configKey: string, defaultValue: any): any => {
  return (config.has(configKey)) ? config.get(configKey) : defaultValue;
};
