import * as fs from 'fs';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import * as rateLimit from 'express-rate-limit';
import * as helmet from 'helmet';
import { AppModule } from './app.module';
import { conf } from './helpers';
import { envOrConf } from './helper/helpers.methods';

const setupSwagger = app => {
  const options = new DocumentBuilder()
    .setTitle(conf('plugins.swagger.title', 'Eatoo'))
    .setVersion('1.8.3')
    .addTag('auth')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup(
    conf('plugins.swagger.path', 'doc'),
    app,
    document,
    {
      customCss: fs.readFileSync('swagger/theme.css').toString(),
    },
  );
};


async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();
  app.use(helmet());
  app.use(rateLimit({
    windowsMs: conf('plugins.rateLimit.windowMs', 900000),
    max: conf('plugins.rateLimit.max', 1000),
  }));
  
  setupSwagger(app);
  
  const port = envOrConf('PORT', 'listenPort', 3000);
  await app.listen(port);
}

bootstrap();
