import { Global, Logger, Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { RedisService } from './redis.service';
import { HelperService } from '../helper/helper.service';
import { conf } from '../helper/helpers.methods';

@Global()
@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'REDIS_SERVICE',
        transport: Transport.TCP,
        options: {
          host: conf('redis.host', '127.0.0.1'),
          port: conf('redis.port', 6373),
        },
      },
    ]),
  ],
  providers: [RedisService],
  exports: [RedisService],
})
export class RedisModule {
  constructor(private readonly redisService: RedisService) {}
  
  async onModuleInit() {
    await this.redisService.connect();
  }
}
