import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { HelperService } from '../helper/helper.service';
import { Observable, PartialObserver } from 'rxjs';

@Injectable()
export class RedisService {
  private readonly confConnRetries: any = false;
  private readonly enabled: boolean = true;
  private online: boolean = false;
  private connRetries: number = 0;
  
  constructor(
    @Inject('REDIS_SERVICE') private readonly client: ClientProxy,
    private readonly helper: HelperService,
  ) {
    this.confConnRetries = this.helper.conf('redis.connectionRetries', false);
    this.enabled = this.helper.conf('redis.enabled', true);
  }
  
  async connect(): Promise<boolean> {
    if (!this.enabled) {
      return false;
    }
    try {
      await this.client.connect();
      this.helper.verbose('Successful connection', 'REDIS', true);
      this.setOnline();
      return true;
    } catch (e) {
      this.helper.error(
        `[${e.code}] Connection failed to Redis server.`,
        'Trace: ' + e.message,
        'REDIS',
        true,
      );
      this.setOnline(false);
      return false;
    }
  }
  
  private canRetryConnection(): boolean {
    if (!this.enabled || this.confConnRetries === false) {
      return false;
    }
    return this.confConnRetries === true || this.connRetries < this.confConnRetries;
  }
  
  setOnline(online: boolean = true) {
    this.online = online;
    if (!this.online) {
      const mode = (this.canRetryConnection()) ? 'RETRY' : 'OFFLINE';
      this.helper.warn(`Redis Service entering ${mode} mode.`, 'REDIS');
    }
  }
  
  private handle<T>(
    caller: Observable<T>,
    observer: PartialObserver<T> = null,
  ): void {
    /**
     * observer should at least have the `complete` property.
     *
     */
    caller.subscribe(observer);
  }
  
  private _emitFail(e: any) {
    this.helper.error(
      `[${e.code}] Cannot emit Redis event.`,
      'Trace: ' + e.message,
      'REDIS',
    );
    this.setOnline(false);
  }
  
  async emit<T>(
    pattern: string,
    data: any = '',
    observer: PartialObserver<T> = {
      complete: () => this.helper.verbose(
        `${pattern} complete.`,
        'REDIS',
      ),
      error: (e: any) => this._emitFail(e),
    },
  ): Promise<any> {
    if (this.online) {
      this.handle<T>(this.client.emit<T>(pattern, data), observer);
      return true;
    } else if (this.canRetryConnection()) {
      if (typeof this.confConnRetries === 'number') {
        this.connRetries += 1;
        this.helper.warn(`[${this.connRetries}] Retrying to connect to Redis...`);
      }
      if (await this.connect()) {
        this.handle<T>(this.client.emit<T>(pattern, data), observer);
        return true;
      }
    }
    if (this.helper.assertConf('redis.debugFails', true)) {
      this.helper.warn(`Event ${pattern} not emitted.`, 'REDIS');
    }
    return false;
  }
}
