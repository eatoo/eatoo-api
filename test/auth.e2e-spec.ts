import { Test, TestingModule } from '@nestjs/testing';
import { expect, assert } from 'chai';
import * as request from 'supertest';
import * as mongoose  from 'mongoose';
import { AppModule } from '../src/app.module';
import { ValidationPipe } from '@nestjs/common';

import RegisterTester from './auth/register.spec';
import LoginTester from './auth/login.spec';
import UpdateTester from './profil/update.spec';

const urls = {
  register: '/auth/register',
  login: '/auth/login',
  update: '/auth/me',
};

const name = Math.random()
.toString(36)
.substring(2, 15) + 
Math.random()
.toString(36)
.substring(2, 15);

const genName = Buffer.from(name).toString('base64');

const userSeed = process.env.TEST_USER_SEED || genName ;

const users = {
  toto: {
    username: `${userSeed}_toto`,
    email: `${userSeed}_toto@eatoo.com`,
    password: userSeed,
  },
  tata : {
    username: `${userSeed}_tata`,
    email: `${userSeed}_tata@eatoo.com`,
    password: userSeed,
  },
  tata1 : {
    email: `${userSeed}_tata1@eatoo.com`,
  },
};

describe('Authentication Tests(e2e)', () => {

  let app;
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  describe('Register Tests(e2e)', () => RegisterTester(urls, users));

  describe('Login Tests(e2e)', () => LoginTester(urls, users));

  describe('Update Tests(e2e)', () => UpdateTester(urls, users));

  afterAll(async () => {
    await app.close();
  });

});
