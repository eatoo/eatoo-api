import { expect, assert } from 'chai';
import * as request from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../src/app.module';
import { ValidationPipe } from '@nestjs/common';

export default (urls, users) => {
  let app;
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  /**
   * Should register a new user : toto
   */
  it('Should register a new user : toto', (done) => {
    request(app.getHttpServer())
      .post(urls.register)
      .send(users.toto)
      .end((err, res) => {
        expect(res.status).to.equal(201);
        expect(res.body).to.have.property('user');
        expect(res.body.user).to.have.property('email').to.equal(users.toto.email);
        done();
      })
    ;
  });

  /**
   * Should fail for register toto
   */
  it('Should fail for register toto', (done) => {
    request(app.getHttpServer())
      .post(urls.register)
      .send(users.toto)
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      })
    ;
  });

  afterAll(async () => {
    await app.close();
  });
};
