import { expect, assert } from 'chai';
import * as request from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../src/app.module';
import { ValidationPipe } from '@nestjs/common';

const auth = { 
  token: ''
}

export default (urls, users) => {
  let app;
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
      
  app = moduleFixture.createNestApplication();
  app.useGlobalPipes(new ValidationPipe());
  await app.init();
  
  const res = await request(app.getHttpServer())
  .post(urls.register)
  .send(users.tata);      
  auth.token = res.body.token;
});

/**
* Should modify the user email : tata
*/
it('Should modify the user email : tata', (done) => {
  request(app.getHttpServer())
  .patch(urls.update)
  .set('Authorization', 'Bearer ' + auth.token)
  .send(users.tata1)
  .end((err, res) => {
    expect(res.status).to.equal(200);
    expect(res.body).to.have.property('username');
    expect(res.body).to.have.property('email');
    done();
  });
});

/**
* Should modify the user email with an existing email
*/
it('Should modify the user email with an existing email', (done) => {
  request(app.getHttpServer())
  .patch(urls.update)
  .set('Authorization', 'Bearer ' + auth.token)
  .send(users.toto)
  .end((err, res) => {
    expect(res.status).to.equal(400);
    expect(res.body.message).to.match(/email/);
    done();
  });
});

/**
* Should modify email with a short email
*/
var emailShort = {
  email : 'tooShort'
}
it('Should modify email with a short email', (done) => {
  request(app.getHttpServer())
  .patch(urls.update)   
  .set('Authorization', 'Bearer ' + auth.token)
  .send(emailShort)
  .end((err, res) => {
    expect(res.status).to.equal(400);
    done();
  });
});

/**
* Should modify email with email non valide
*/
var emailFake = {
  email : 'errorepitech.com'
}
it('Should modify email with email non valide', (done) => {
  request(app.getHttpServer())
  .patch(urls.update)   
  .set('Authorization', 'Bearer ' + auth.token)
  .send(emailFake)
  .end((err, res) => {
    expect(res.status).to.equal(400);
    done();
  });
});

afterAll(async () => {
  await app.close();
});
};